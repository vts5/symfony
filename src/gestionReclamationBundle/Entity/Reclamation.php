<?php

namespace gestionReclamationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
// src/AppBundle/Entity/client.php
/**
 * Reclamation
 *
 * @ORM\Table(name="Reclamation")
 * @ORM\Entity(repositoryClass="gestionReclamationBundle\Repository\reclamationRepository")
 */
class Reclamation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return reclamation
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return reclamation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="reclamations")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $client;


    /**
     * Set client
     *
     * @param \gestionReclamationBundle\Entity\Client $client
     *
     * @return reclamation
     */
    public function setClient(\gestionReclamationBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \gestionReclamationBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

}
